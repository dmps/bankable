const async = require('async');
const parse = require('csv-parse');
const request = require('request');
const geolib = require('geolib')
const fs = require('fs');

function getMap(lat, lon) {
    return `https://www.google.com/maps/dir/Current+Location/${lat},${lon}`
}

exports.handler = (event, context, callback) => {
    // TODO implement
    console.log(`Call from ${event.lat},${event.lon} who wants to ${event.action} and has a referral: ${event.referral}`)
    async.waterfall([function (cb) {
        var locations = []
        var parser = parse({ delimiter: ',' }, function (err, data) {
            data.forEach(function (row) {
                console.log(row)
                if (row.length === 3) {
                    locations[row[0]] = { latitude: parseFloat(row[1]), longitude: parseFloat(row[2]), phoneNumber: row[3] }
                } else {
                    console.log('CAB row found')
                    locations[row[0]] = { latitude: parseFloat(row[1]), longitude: parseFloat(row[2]), phoneNumber: row[3], CAB: row[4] }
                }

            }, this);
            cb(null, locations)
        });
        if (event.action === 'collect') {
            fs.createReadStream(`${__dirname}/${event.action}-${event.referral}.csv`).pipe(parser);
        } else {
            fs.createReadStream(`${__dirname}/${event.action}.csv`).pipe(parser);
        }
    }, function (locations, cb) {
        cb(null, geolib.findNearest({ latitude: event.lat, longitude: event.lon }, locations))
    }], function (err, result) {
        console.log(Object.keys(result).length)
        if (result.CAB) {
            callback(null, {
                name: result.key,
                phoneNumber: result.phoneNumber,
                lat: result.latitude,
                lon: result.longitude,
                distance: result.distance,
                CAB: result.CAB,
                map: getMap(result.latitude, result.longitude)
            })
        }else{
            callback(null, {
                name: result.key,
                phoneNumber: result.phoneNumber,
                lat: result.latitude,
                lon: result.longitude,
                distance: result.distance,
                map: getMap(result.latitude, result.longitude)
            })
        }
        
    })
};