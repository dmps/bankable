#Bankable

##Donate to or Collect from Your Nearest Food Bank in a Flash

##Name: Dalton Scott

##Problem: 

* Very few people know where they can donate to a food bank
* Even if they do donate, very few people know where they can collect emergency food supplies
* Many food banks require referrals in order to collect food supplies
* Many of those in need have no idea of where to get a referral


##Market: 

* Between 1st April 2016 and 31st March 2017, The Trussell Trust (one of the larger food bank networks) provided 1,182,954 three day emergency food supplies to people in crisis.
* Of this number, 436,938 went to children.
* 391 people died from malnutrition in the UK 2015 (ONS)


##Solution:

An Alexa Skill which helps the user:

* Find their nearest food bank which accepts donations.
* Find their nearest food bank which does not require referrals.
* Get a referral for the food banks which require it.
* Find their nearest food bank which requires a referral, if they have it.

##Benefits:

* Faster for donors to find their nearest food bank
* User saves time that would have been spent going to a food bank only to be asked for a referral

* Users without a referral are only sent to food banks that do not require a referral


##Development:

Bankable is split into two parts: the skill and the locations API. The skill uses the Node.js alexa-sdk to guide the user through the user journey (see PDF). Once the skill has compiled enough information about the user, we collect their location from the Alexa app and send a HTTP post request to the Bankable locations API through API Gateway and the request library. The API receives the request loads and reads the corresponding CSV file, then sends back the closest location in the file with its corresponding information to the skill. The skill then tells the user the name of their closest food bank and sends further details (directions,distance away, etc.) to their Alexa App as a card. Git was used throughout so you can see the development of the code in BitBucket. We also used BitBucket Pipelines to publish to AWS Lambda

##Technologies Used: Node.js,ES6,API.AI, AWS API Gateway, AWS Lambda,Git, Bitbucket Piplines, Linux, NPM, Async, Geolib, Request