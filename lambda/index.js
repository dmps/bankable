const Alexa = require('alexa-sdk');
const AlexaDeviceClient = require('./AlexaDeviceClient');
const request = require('request');
const async = require('async');


exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context, callback);
    alexa.registerHandlers(handlers);
    alexa.execute();
};

const PERMISSIONS = ["read::alexa:device:all:address:country_and_postal_code"];

//Gets a foodbank back from the API
function getFoodBank(postcode,action,referral,callback) {
    const endpoint = 'https://ks21u0t4zj.execute-api.eu-west-1.amazonaws.com/alpha/nearest';
    async.waterfall([
        function (callback) {   
            request.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAvhdy0o2oTc7XkmgpBn8O_xhoI-c57z8M&address=' + encodeURIComponent(postcode), function (err, res, body) {
                body = JSON.parse(body);
                var geo = {
                    "lat": body.results[0].geometry.location.lat,
                    "lon": body.results[0].geometry.location.lng,
                    "action":action,
                    "referral":referral
                }
                console.log(body)
                callback(err, geo)
            });
        },
        function (geo, callback) {
            // create the options var
            var options = {
                method: 'POST',
                headers: {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json'
                },
                body: geo,
                json: true,
                url: endpoint
            }
            callback(null, options);
        },
        function (options, callback) {
            // Make the request
            request(options, function (err, response, body) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, body)
                }
            })
        }
    ], callback)
}



var handlers = {
    'LaunchRequest': function () {
        this.emit('WelcomeIntent');
    },
    'WelcomeIntent': function () {
        this.emit(':ask', 'Welcome to Bankable, we help you find your nearest food bank in a flash! Would you like to donate or collect some food?');
    },
    'AMAZON.YesIntent': function () { //they have a referral
        var self = this;
        getFoodBank(this.attributes.postcode,'collect',true,(err,res)=>{
            if (err) console.error(err)
            self.emit(':tellWithCard',`Your closest food bank is ${res.name}. More information will be sent to your Alexa App. Thank you for using Bankable.`,`Your Nearest Food Bank: ${res.name}`,`Directions: ${res.map} Phone Number: ${res.phoneNumber}`)
        });
    },
    'AMAZON.NoIntent': function () { //they do not have a referral
        var self = this;
        getFoodBank(this.attributes.postcode,'collect',false,(err,res)=>{
            if (err) console.error(err)
            self.emit(':tellWithCard',`Your closest food bank is ${res.name}. More information will be sent to your Alexa App. Thank you for using Bankable.`,`Your Nearest Food Bank: ${res.name}`,`Directions: ${res.map} Phone Number: ${res.phoneNumber} Local Citizen's Advice Bureau: ${res.CAB}`)
        });
    },
    'ActionIntent': function () {
        const action = this.event.request.intent.slots.ACTION.value
        var deviceId = this.event.context.System.device.deviceId;
        var apiEndpoint = this.event.context.System.apiEndpoint;
        console.log(apiEndpoint,deviceId)
        if (!this.event.context.System.user.permissions.consentToken) {
            this.emit(":tellWithPermissionCard", 'Please give us permission to use your postcode in the Alexa app', PERMISSIONS)
            console.log('no permissions')
        } else {
            console.log(`ConsentToken: ${this.event.context.System.user.permissions.consentToken}`)
            var self = this;
            const consentToken = this.event.context.System.user.permissions.consentToken;
            const alexaDeviceAddressClient = new AlexaDeviceClient(apiEndpoint, deviceId, consentToken)
            const postcodeRequest = alexaDeviceAddressClient.getCountryAndPostalCode()
            postcodeRequest.then((response) => {
                console.log(response.address.postalCode)
                switch (response.statusCode) {
                    case 200:
                        // this.emit(':tell', `Your address is ${response.address.postalCode} and your action is ${action}`)
                        this.attributes.postcode = response.address.postalCode
                        var self = this;
                        if(action==='donate'){ //send their nearesest donation location
                            getFoodBank(response.address.postalCode,action,false,(err,res)=>{
                                if (err) console.error(err)
                                self.emit(':tellWithCard',`Your closest food bank is ${res.name}. More information will be sent to your Alexa App. Thank you for using Bankable.`,`Your Nearest Food Bank: ${res.name}`,`Directions: ${res.map} Phone Number: ${res.phoneNumber}`)
                            });
                        }else{ //check if they have a referral
                            this.emit(':ask',"Do you have a referral from your social worker or local Citizen's Advice Bureau?")
                        }
                        break;
                    case 204: //no address in the companion app
                        this.emit(':tell', 'Please set your postcode in the Alexa App in order to use this service.');
                        break;
                    case 403: //consent token is false
                        this.emit(":tellWithPermissionCard", 'Please give us permission to use your postcode in the Alexa app', PERMISSIONS)
                    default:
                        this.emit(":tell", 'There was an unexpected error. Please try again some other time.')
                        break;
                }
            })
        }
    },
    'ExitIntent': function () {
        this.emit(':tell', 'Thank you for using Bankable. We look forward to your next visit!');
    },
    'AMAZON.CancelIntent': function () {
        this.emit('ExitIntent');
    },
    'AMAZON.StopIntent': function () {
        this.emit('ExitIntent');
    },
    'AMAZON.HelpIntent': function () {
        this.emit(':ask', 'If you would like, we can find the closest food bank to the address saved on your Alexa device. You just need to reply with yes or no. Would you like us to find your nearest food bank?');
    },
    'Unhandled': function () {
        this.emit(':ask','Would you like us to find your nearest food bank?')
    }
};